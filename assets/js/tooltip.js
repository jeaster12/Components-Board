

/*=========================================
 * Tooltips
=========================================*/
$(function() {
	
	
	$('#warning').tooltipster({
		content: $('<div style="float:left; margin:12px;"><span class="fa fa-exclamation-triangle fa-2x" style="color:#FFC956;"></span></div><div style="float:right"><p style="text-align:left; color:#FFC956; margin-top:2px;"><strong>CUSIPs have been sent and <br/> received via the API.</strong></p><p style="font-size:12px; font-style:italic; color:#ccc; padding-top: 0px;">Applicake lollipop oat cake gingerbread.</p></div>'),
		// setting a same value to minWidth and maxWidth will result in a fixed width
		minWidth: 300,
		maxWidth: 300,
        theme: 'tooltipster-shadow',
        animation: 'grow',
		position: 'right'
	});
    
    $('#error').tooltipster({
		content: $('<div style="float:left; margin:12px;"><span class="fa fa-exclamation-triangle fa-2x" style="color:#d15256;"></span></div><div style="float:right"><p style="text-align:left; color:#d15256; margin-top:2px;"><strong>CUSIPs have been sent and <br/> received via the API.</strong></p><p style="font-size:12px; font-style:italic; color:#ccc; padding-top: 0px;">Applicake lollipop oat cake gingerbread.</p></div>'),
		// setting a same value to minWidth and maxWidth will result in a fixed width
		minWidth: 300,
		maxWidth: 300,
        theme: 'tooltipster-shadow',
        animation: 'grow',
		position: 'right'
	});



	$('.tooltipster-shadow-preview').tooltipster({
		theme: 'tooltipster-shadow'
	});
	

	
});






