document.addEventListener('DOMContentLoaded', function() {

	[].forEach.call(document.querySelectorAll('.push'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.buttons'), function(el) {
				el.classList.add("animated", "fadeInRight", "showme");
			});
		});
	});
	
	[].forEach.call(document.querySelectorAll('.push'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('checker').classList.toggle('hidden');
		});
	});
	
	[].forEach.call(document.querySelectorAll('.one-button'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('checker').classList.toggle('hidden');
		});
	});
	[].forEach.call(document.querySelectorAll('.one-button'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.top-table'), function(el) {
				el.classList.add('not-allowed');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.one-button'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.buttons'), function(el) {
				el.classList.toggle('hidden');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.one-button'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('checkme').classList.add('not-allowed');
		});
	});
	[].forEach.call(document.querySelectorAll('.one-button'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('no').classList.add('bg-not-allowed');
		});
	});
	[].forEach.call(document.querySelectorAll('.two'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('go').classList.add('hidden');
		});
	});
	[].forEach.call(document.querySelectorAll('.two'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('gone').classList.add('bg2');
		});
	});
	[].forEach.call(document.querySelectorAll('.two'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('stay').classList.add('unhidden');
		});
	});
	[].forEach.call(document.querySelectorAll('.two'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('gone').classList.add('animated');
		});
	});
	[].forEach.call(document.querySelectorAll('.two'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('gone').classList.add('fadeInUp');
		});
	});
	[].forEach.call(document.querySelectorAll('.two-button'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('go').classList.toggle('hidden');
		});
	});
	[].forEach.call(document.querySelectorAll('.two-button'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('gone').classList.add('bg-not-allowed');
		});
	});
	[].forEach.call(document.querySelectorAll('.two-button'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('stay').classList.toggle('hidden');
		});
	});
	[].forEach.call(document.querySelectorAll('.two-button'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('go').classList.add("not-allowed", "animated", "fadeInUp");
		});
	});
	
	[].forEach.call(document.querySelectorAll('.two-button'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('gone').classList.remove('bg2');
		});
	});
});