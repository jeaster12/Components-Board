/*=========================================
 * Cusip Registration Forms
=========================================*/
document.addEventListener('DOMContentLoaded', function() {
	[].forEach.call(document.querySelectorAll('.change-button'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.content'), function(el) {
				el.classList.add('animated');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.change-button'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.content'), function(el) {
				el.classList.add('fadeIn');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.change-button'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.content'), function(el) {
				el.classList.remove('hide');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.change-button'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.mids'), function(el) {
				el.classList.add('hide');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.change-button'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.expander'), function(el) {
				el.classList.add('hide');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.change-button'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.close2'), function(el) {
				el.classList.remove('hide');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.close2'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.content'), function(el) {
				el.classList.add('hide');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.close2'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.close2'), function(el) {
				el.classList.add('hide');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.close2'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.mids'), function(el) {
				el.classList.remove('hide');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.close2'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.expander'), function(el) {
				el.classList.remove('hide');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.account-card-empty'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.content2'), function(el) {
				el.classList.add('animated');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.account-card-empty'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.content2'), function(el) {
				el.classList.add('fadeIn');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.account-card-empty'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.content2'), function(el) {
				el.classList.remove('hide');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.account-card-empty'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.content'), function(el) {
				el.classList.add('hide');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.close2'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.content2'), function(el) {
				el.classList.add('animated');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.close2'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.content2'), function(el) {
				el.classList.add('fadeOutUp');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.close2'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.content2'), function(el) {
				el.classList.add('hide');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.close2'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.mids'), function(el) {
				el.classList.remove('hide');
			});
		});
	});
	[].forEach.call(document.querySelectorAll('.close2'), function(el) {
		el.addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.expander'), function(el) {
				el.classList.remove('hide');
			});
		});
	});
	document.getElementById('selectme').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.content'), function(el) {
				el.classList.add('hide');
			});
	});
	document.getElementById('selectme').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.close2'), function(el) {
				el.classList.add('hide');
			});
	});
	document.getElementById('selectme').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.mids'), function(el) {
				el.classList.remove('hide');
			});
	});
	document.getElementById('selectme').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.expander'), function(el) {
				el.classList.remove('hide');
			});
	});
	document.getElementById('selectme2').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.content'), function(el) {
				el.classList.add('hide');
			});
	});
	document.getElementById('selectme2').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.close2'), function(el) {
				el.classList.add('hide');
			});
	});
	document.getElementById('selectme2').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.expander'), function(el) {
				el.classList.remove('hide');
			});
	});
	document.getElementById('selectme2').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('first').classList.remove('account-card-primary');
	});
	document.getElementById('selectme2').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('first').classList.add('account-card');
	});
	document.getElementById('selectme2').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('second').classList.remove('account-card');
	});
	document.getElementById('selectme2').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			document.getElementById('second').classList.add('account-card-primary');
	});
	document.getElementById('selectme2').addEventListener('click', function(e) {
			if (e.target.nodeName.toLowerCase() == 'a') {
				e.preventDefault();
			}
			[].forEach.call(document.querySelectorAll('.mids2'), function(el) {
				el.classList.remove('hide');
			});
	});
});
